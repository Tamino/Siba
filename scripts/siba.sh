#!/bin/sh

# Set the current directory with the Siba directory.
cd "`dirname "$0"`"

#
java -Xms368m -Xmx1024m -jar siba.jar $@
