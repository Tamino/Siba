/*
 * Copyright (C) 2016 Didier Clermonté <dclermonte@april.org>
 * Copyright (C) 2016 Christian Pierre Momon <christian.momon@devinsy.fr>
 *
 * This file is part of Siba.
 *
 * Siba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.dclermonte.siba;

/**
 *
 * @author Didier Clermonté (dclermonte@april.org)
 *
 */
public class SibaException extends Exception
{
    private static final long serialVersionUID = 2909630770291570845L;

    /**
    *
    */
    public SibaException()
    {
        super();
    }

    /**
     *
     * @param message
     *            error message
     */
    public SibaException(final String message)
    {
        super(message);
    }

    /**
     *
     * @param message
     *            text
     * @param cause
     *            caused by…
     */
    public SibaException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     *
     * @param cause
     *            caused by …
     */
    public SibaException(final Throwable cause)
    {
        super(cause);
    }
}