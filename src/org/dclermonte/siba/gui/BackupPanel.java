/*
 * Copyright (C) 2016 Didier Clermonté <dclermonte@april.org>
 * Copyright (C) 2016 Christian Pierre Momon <christian.momon@devinsy.fr>
 *
 * This file is part of Siba.
 *
 * Siba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dclermonte.siba.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.dclermonte.siba.SibaException;
import org.dclermonte.siba.model.SibaManager;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

/**
 *
 * This class is the panel for backup.
 *
 * @author dclermonte
 *
 */
public class BackupPanel extends JPanel
{
    private static final long serialVersionUID = 4714383090458639282L;
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.dclermonte.siba.gui.messages");
    private JTextField sourceDirectoryField;
    private JTextField targetDirectoryField;

    /**
     * This is the constructor for this panel.
     *
     * @throws SibaException
     *             specific exception
     */
    public BackupPanel() throws SibaException
    {

        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        setLayout(new BorderLayout(0, 0));

        JPanel helpPanel = new JPanel();
        add(helpPanel, BorderLayout.NORTH);

        JLabel lblHelp = new JLabel(BUNDLE.getString("BackupPanel.lblNewLabel_2.text"));
        helpPanel.add(lblHelp);

        JPanel choosePanel = new JPanel();
        add(choosePanel);
        choosePanel.setLayout(new FormLayout(
                new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
                        FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormSpecs.RELATED_GAP_COLSPEC,
                        FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC, },
                new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, }));

        JLabel lblSourceDirectory = new JLabel(BUNDLE.getString("BackupPanel.lblSourceDirectory.text"));
        choosePanel.add(lblSourceDirectory, "2, 2");
        lblSourceDirectory.setHorizontalAlignment(SwingConstants.LEFT);
        lblSourceDirectory.setVerticalAlignment(SwingConstants.TOP);

        this.sourceDirectoryField = new JTextField();
        choosePanel.add(this.sourceDirectoryField, "4, 2");
        this.sourceDirectoryField.setColumns(10);

        JButton btnSourceDirectory = new JButton(BUNDLE.getString("BackupPanel.btnNewButton.text"));
        btnSourceDirectory.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        btnSourceDirectory.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                File file = SourceDirectorySelector.showSelectorDialog(BackupPanel.this, null);
                if (file != null)
                {
                    BackupPanel.this.sourceDirectoryField.setText(file.getPath());
                }
            }
        });
        choosePanel.add(btnSourceDirectory, "6, 2");

        JLabel lblTargetDirectory = new JLabel(BUNDLE.getString("BackupPanel.lblTargetDirectory.text"));
        choosePanel.add(lblTargetDirectory, "2, 4");
        lblTargetDirectory.setVerticalAlignment(SwingConstants.TOP);
        lblTargetDirectory.setHorizontalAlignment(SwingConstants.LEFT);

        this.targetDirectoryField = new JTextField();
        choosePanel.add(this.targetDirectoryField, "4, 4");
        this.targetDirectoryField.setColumns(10);

        JButton btnTarget = new JButton(BUNDLE.getString("BackupPanel.btnNewButton_1.text"));
        btnTarget.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        btnTarget.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                File file = TargetDirectorySelector.showSelectorDialog(BackupPanel.this, null);
                if (file != null)
                {
                    BackupPanel.this.targetDirectoryField.setText(file.getPath());
                }
            }
        });
        choosePanel.add(btnTarget, "6, 4");

        btnTarget.setHorizontalAlignment(SwingConstants.RIGHT);

        JPanel actionPanel = new JPanel();

        add(actionPanel, BorderLayout.SOUTH);
        actionPanel.setLayout(new BoxLayout(actionPanel, BoxLayout.Y_AXIS));

        Component verticalStrut1 = Box.createVerticalStrut(5);
        actionPanel.add(verticalStrut1);

        JPanel buttonActionPanel = new JPanel();
        actionPanel.add(buttonActionPanel);
        buttonActionPanel.setLayout(new BoxLayout(buttonActionPanel, BoxLayout.X_AXIS));

        Component horizontalGlue = Box.createHorizontalGlue();
        buttonActionPanel.add(horizontalGlue);

        JButton btnBackup = new JButton(BUNDLE.getString("BackupPanel.btnNewButton_2.text"));
        btnBackup.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        buttonActionPanel.add(btnBackup);

        Component horizontalGlue1 = Box.createHorizontalGlue();
        buttonActionPanel.add(horizontalGlue1);

        Component verticalStrut = Box.createVerticalStrut(20);
        actionPanel.add(verticalStrut);
        btnBackup.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                try
                {
                    File directoryToSave1 = new File(BackupPanel.this.sourceDirectoryField.getText());
                    File targetDirectory = new File(BackupPanel.this.targetDirectoryField.getText());
                    if (directoryToSave1.exists() && targetDirectory.exists())
                    {
                        File output1 = SibaManager.backup(directoryToSave1, targetDirectory);

                        String message = BUNDLE.getString("BackupPanel.confirmDialogMessage.text") + output1.getName();
                        String titre = BUNDLE.getString("BackupPanel.confirmDialogTitle.text");
                        JOptionPane.showMessageDialog(BackupPanel.this, message, titre,
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                    else
                    {
                        String dataMissing = BUNDLE.getString("BackupPanel.dataMissing.text");
                        String titleWarning = BUNDLE.getString("BackupPanel.TitleWarning.text");
                        JOptionPane.showMessageDialog(BackupPanel.this, dataMissing, titleWarning,
                                JOptionPane.INFORMATION_MESSAGE);

                    }
                }
                catch (SibaException exception)
                {
                    String dataMissing = exception.getMessage();

                    String titleWarning = BUNDLE.getString("BackupPanel.TitleWarning.text");
                    JOptionPane.showMessageDialog(BackupPanel.this, dataMissing, titleWarning,
                            JOptionPane.INFORMATION_MESSAGE);
                }

            }
        });
    }

    /**
     *
     * @param sourceDirectory
     *            the source Directory
     */
    public void setSourceDirectory(final String sourceDirectory)
    {
        this.sourceDirectoryField.setText(sourceDirectory);
    }
}
