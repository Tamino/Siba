/*
 * Copyright (C) 2016 Didier Clermonté <dclermonte@april.org>
 * Copyright (C) 2016 Christian Pierre Momon <christian.momon@devinsy.fr>
 *
 * This file is part of Siba.
 *
 * Siba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.dclermonte.siba.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.dclermonte.siba.SibaException;
import org.dclermonte.siba.model.SibaManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

/**
 *
 * @author Didier Clermonté (dclermonte@april.org)
 *
 */

public class CheckPanel extends JPanel
{
    private static final long serialVersionUID = 4495957907349664847L;
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.dclermonte.siba.gui.messages");
    private static Logger logger = LoggerFactory.getLogger(CheckPanel.class);
    private JTextField fileToCheckField;

    /**
     *
     * @throws SibaException
     *             specific exception
     */
    public CheckPanel() throws SibaException
    {
        setLayout(new BorderLayout(0, 0));

        JPanel choosePanel = new JPanel();
        add(choosePanel, BorderLayout.CENTER);
        choosePanel.setLayout(new FormLayout(
                new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
                        FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormSpecs.RELATED_GAP_COLSPEC,
                        FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC, },
                new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, }));

        JLabel lblNewLabel = new JLabel(BUNDLE.getString("CheckPanel.lblNewLabel.text"));
        lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
        choosePanel.add(lblNewLabel, "2, 2, left, center");

        JButton btnNewButton = new JButton(BUNDLE.getString("CheckPanel.btnNewButton.text"));
        btnNewButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                File choosenFile = CheckDirectorySelector.showSelectorDialog(CheckPanel.this, null);
                if ((choosenFile != null) && choosenFile.exists())
                {
                    CheckPanel.this.fileToCheckField.setText(choosenFile.getAbsolutePath());
                    CheckPanel.this.fileToCheckField
                            .setCaretPosition(CheckPanel.this.fileToCheckField.getText().length());
                }
            }
        });

        this.fileToCheckField = new JTextField();
        choosePanel.add(this.fileToCheckField, "4, 2, fill, default");
        this.fileToCheckField.setColumns(10);
        btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        choosePanel.add(btnNewButton, "6, 2, left, top");

        JPanel actionPanel = new JPanel();
        add(actionPanel, BorderLayout.SOUTH);
        actionPanel.setLayout(new BoxLayout(actionPanel, BoxLayout.Y_AXIS));

        Component verticalStrut = Box.createVerticalStrut(20);
        actionPanel.add(verticalStrut);

        JPanel panel = new JPanel();
        actionPanel.add(panel);
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

        Component horizontalGlue = Box.createHorizontalGlue();
        panel.add(horizontalGlue);
        JButton btnCheck = new JButton(BUNDLE.getString("CheckPanel.btnNewButton_1.text"));
        panel.add(btnCheck);
        btnCheck.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                try
                {
                    File choosenFile = new File(CheckPanel.this.fileToCheckField.getText());
                    if ((choosenFile != null) && choosenFile.exists())
                    {
                        boolean check = SibaManager.check(choosenFile);
                        String titre = BUNDLE.getString("CheckPanel.confirmDialogTitle.text");
                        if (check)
                        {
                            String message = String.format(BUNDLE.getString("CheckPanel.confirmDialogGood.text"),
                                    choosenFile.getName());
                            JOptionPane.showMessageDialog(CheckPanel.this, message, titre,
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        else
                        {
                            String message = String.format(BUNDLE.getString("CheckPanel.confirmDialogBad.text"),
                                    choosenFile.getName());
                            JOptionPane.showMessageDialog(CheckPanel.this, message, titre,
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                    else
                    {
                        String dataMissing = BUNDLE.getString("BackupPanel.dataMissing.text");
                        String titleWarning = BUNDLE.getString("BackupPanel.TitleWarning.text");
                        JOptionPane.showMessageDialog(CheckPanel.this, dataMissing, titleWarning,
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                catch (SibaException exception)
                {
                    logger.error("SibaException ", exception);
                }
            }
        });
        btnCheck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        Component horizontalGlue1 = Box.createHorizontalGlue();
        panel.add(horizontalGlue1);
        Component verticalStrut1 = Box.createVerticalStrut(20);
        actionPanel.add(verticalStrut1);
    }

    /**
     *
     * @param sourceDirectory
     *            the source directory
     */
    public void setFileToCheck(final String sourceDirectory)
    {
        this.fileToCheckField.setText(sourceDirectory);
    }
}
