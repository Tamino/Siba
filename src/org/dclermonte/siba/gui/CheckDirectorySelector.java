/*
 * Copyright (C) 2016 Didier Clermonté <dclermonte@april.org>
 * Copyright (C) 2016 Christian Pierre Momon <christian.momon@devinsy.fr>
 *
 * This file is part of Siba.
 *
 * Siba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dclermonte.siba.gui;

import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.util.ResourceBundle;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 *
 */
public class CheckDirectorySelector extends JFileChooser
{
    private static final long serialVersionUID = 3782597353602048214L;
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.dclermonte.siba.gui.messages");
    private static Logger logger = LoggerFactory.getLogger(CheckDirectorySelector.class);

    /**
     *
     * @param targetFile
     *            target file
     */
    public CheckDirectorySelector(final File targetFile)
    {
        super();
        setBackground(new Color(152, 251, 152));
        //
        File file;
        if ((targetFile == null) || (StringUtils.isBlank(targetFile.getAbsolutePath())))
        {
            file = null;
        }
        else if (targetFile.isFile())
        {
            file = targetFile.getParentFile();
        }
        else
        {
            file = targetFile;
        }

        //
        setSelectedFile(file);
        setDialogTitle(BUNDLE.getString("CheckDirectorySelector.this.dialogTitle"));
        setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Fichiers md5", "md5");
        setAcceptAllFileFilterUsed(false);
        addChoosableFileFilter(filter);
        setApproveButtonText(BUNDLE.getString("BackupPanel.checkFileChooserButton.text"));
        setDialogType(CUSTOM_DIALOG);
    }

    /**
     *
     */
    @Override
    public final void setSelectedFile(final File file)
    {
        //
        super.setSelectedFile(file);
        logger.debug("==== SET SELECTED FILE=================");
        logger.debug("SELECED FILE  {}", file);
    }

    /**
     *
     */
    @Override
    public void setVisible(final boolean visible)
    {
        //
        super.setVisible(visible);

        if (!visible)
        {
            //
            resetChoosableFileFilters();
        }
    }

    /**
     * This method is the main one of the selector.
     *
     * @param parent
     *            The parent's component
     * @param targetFile
     *            target file
     * @return the selected file
     */
    public static File showSelectorDialog(final Component parent, final File targetFile)
    {
        File result;

        //
        CheckDirectorySelector selector = new CheckDirectorySelector(targetFile);

        //
        if (selector.showDialog(parent, null) == JFileChooser.APPROVE_OPTION)
        {
            //
            result = selector.getSelectedFile();

        }
        else
        {
            //
            result = null;
        }

        //
        return result;
    }
}