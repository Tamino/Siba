/*
 * Copyright (C) 2016 Didier Clermonté <dclermonte@april.org>
 * Copyright (C) 2016 Christian Pierre Momon <christian.momon@devinsy.fr>
 *
 * This file is part of Siba.
 *
 * Siba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dclermonte.siba.cli;

import java.io.File;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.dclermonte.siba.SibaException;
import org.dclermonte.siba.model.SibaManager;
import org.dclermonte.siba.util.BuildInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.strings.StringList;

/**
 * This class CLI is for use from Command Line Interface.
 *
 *
 */
public final class SibaCLI
{
    private static Logger logger = LoggerFactory.getLogger(SibaCLI.class);
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.dclermonte.siba.cli.messages");

    /**
     *
     */
    private SibaCLI()
    {
    }

    /**
     *
     * @param directoryToSave
     *            the name of the directory to save
     * @throws SibaException
     *             exception file not found
     */
    public static void backup(final File directoryToSave) throws SibaException
    {
        File result;
        result = SibaManager.backup(directoryToSave);
        System.out.println(BUNDLE.getString("confirmDialog.text") + result.getName());
    }

    /**
     *
     * This method is called for Backup from CLI.
     *
     * @param directoryToSave
     *            The directory to save
     * @param targetDirectory
     *            where to put the generated file
     * @throws SibaException
     *             specific exception
     */
    public static void backup(final File directoryToSave, final File targetDirectory) throws SibaException
    {
        File result;
        result = SibaManager.backup(directoryToSave, targetDirectory);
        System.out.println(BUNDLE.getString("confirmDialog.text") + result.getName());
    }

    /**
     *
     * This method is called for Check from CLI.
     *
     * @param fileToCheck
     *            The file to be check
     * @throws SibaException
     *             specific exception file not found
     */
    public static void check(final File fileToCheck) throws SibaException
    {
        if (SibaManager.check(fileToCheck))
        {
            String message = String.format(BUNDLE.getString("confirmDialogGood.text"), fileToCheck.getName());
            System.out.println(message);
        }
        else
        {
            String message = String.format(BUNDLE.getString("confirmDialogBad.text"), fileToCheck.getName());
            System.out.println(message);
        }
    }

    /**
     * This method send help response.
     *
     */
    public static void help()
    {
        StringList message = new StringList();

        message.append("Siba version ").appendln(new BuildInformation().version());
        message.appendln("Usage:");
        message.appendln("    siba [ -h | -help | --help ]");
        message.appendln("    siba backup directoryToSave [ target ]");
        message.appendln("    siba check [ filename | filename.tgz | filename.tgz.md5 ]");

        System.out.println(message.toString());
    }

    /**
     *
     */
    public static void manageSibaCLIException()
    {
        // Set default CLI catch.
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException(final Thread thread, final Throwable exception)
            {
                String message;
                if (exception instanceof OutOfMemoryError)
                {
                    message = "Java ran out of memory!\n\n";
                }
                else
                {
                    message = String.format("An error occured: %1s(%2s)", exception.getClass(), exception.getMessage());
                }

                logger.error("uncaughtException ", exception);
                logger.error(message);
                logger.info(BUNDLE.getString("UnexpectedError.text"));
            }
        });
    }

    /**
     *
     * This method launch CLI.
     *
     * @param args
     *            necessary arguments
     */
    public static void run(final String[] args)
    {
        try
        {
            manageSibaCLIException();
            // This part implements an automate.
            int parameterCount = args.length;
            if (parameterCount == 0)
            {
                help();
            }
            else if (StringUtils.equals(args[0], "-h") || StringUtils.equals(args[0], "-help")
                    || StringUtils.equals(args[0], "--help"))
            {
                help();
            }
            else if (StringUtils.equals(args[0], "backup"))
            {
                if (parameterCount == 1)
                {
                    backup(null, null);
                }
                else if (parameterCount == 2)
                {
                    backup(new File(args[1]));
                }
                else if (parameterCount == 3)
                {
                    backup(new File(args[1]), new File(args[2]));
                }
                else
                {
                    throw new SibaException(BUNDLE.getString("ToManyParameter.text"));
                }
            }
            else if (StringUtils.equals(args[0], "check"))
            {
                if (parameterCount == 1)
                {
                    check(null);
                }
                else if (parameterCount == 2)
                {
                    String fileToCheckName;
                    if (!args[1].endsWith("tgz") && !args[1].endsWith("md5"))
                    {
                        fileToCheckName = args[1].concat(".tgz.md5");
                    }
                    else if (args[1].endsWith("tgz"))
                    {
                        fileToCheckName = args[1].concat(".md5");
                    }
                    else
                    {
                        fileToCheckName = args[1];
                    }
                    File fileToCheck = new File(fileToCheckName);
                    check(fileToCheck);
                }
                else
                {
                    throw new SibaException(BUNDLE.getString("ToManyParameter.text"));
                }
            }
            else
            {
                System.out.println(BUNDLE.getString("badUsage.text"));
                help();
            }
        }
        catch (SibaException exception)
        {
            System.err.println("SibaException = " + exception.getMessage());
            logger.error(exception.getMessage(), exception);
            help();
        }
    }
}
