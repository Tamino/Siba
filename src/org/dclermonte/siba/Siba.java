/*
 * Copyright (C) 2016 Didier Clermonté <dclermonte@april.org>
 * Copyright (C) 2016 Christian Pierre Momon <christian.momon@devinsy.fr>
 *
 * This file is part of Siba.
 *
 * Siba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dclermonte.siba;

import java.io.File;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.dclermonte.siba.cli.SibaCLI;
import org.dclermonte.siba.gui.SibaGUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class Siba stands for Simple Backup. This is the main class.
 *
 *
 */
public final class Siba
{
    private static Logger logger = LoggerFactory.getLogger(Siba.class);

    /**
     *
     */
    private Siba()
    {
    }

    /**
     *
     * @param args
     *            arguments if no argument launch GUI else CLI
     */
    public static void main(final String[] args)
    {
        // Configure log.
        File loggerConfig = new File("log4j.properties");
        if (loggerConfig.exists())
        {
            PropertyConfigurator.configure(loggerConfig.getAbsolutePath());
            logger.info("Dedicated log configuration done.");
            logger.info("Configuration file was found in [{}].", loggerConfig.getAbsoluteFile());
        }
        else
        {
            BasicConfigurator.configure();
            logger.info("Basic log configuration done.");
            logger.info("Configuration file was not found in [{}].", loggerConfig.getAbsoluteFile());
        }

        // Run.
        if (args.length == 0)
        {
            SibaGUI.run();
        }
        else
        {
            SibaCLI.run(args);
        }
    }
}
